from django.shortcuts import render
from django.views.generic import ListView
from .models import Sales
from invoice.models import InvoiceId, InvoiceRow
# Create your views here.


class TotalSalesView(ListView):
    model = Sales
    template_name = 'sales/all_sales.html'


def individual_sales_detail(request, id):
    sales_info = Sales.objects.get(id=id)
    invoice_id = sales_info.invoice_id
    a = InvoiceRow.objects.filter(invoice_id=invoice_id)
    total = 0
    for i in a:
        total += i.price

    return render(request, 'sales/particular_sales_detail.html', {'query_set': a, 'invoice_id' : invoice_id, 'total':total})

