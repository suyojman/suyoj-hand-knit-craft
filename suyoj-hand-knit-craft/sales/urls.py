from django.urls import path
from . import views


urlpatterns = [

    path('record/', views.TotalSalesView.as_view(), name="total_sales"),
    path('record/<int:id>', views.individual_sales_detail, name="individual_sales_detail"),

]
