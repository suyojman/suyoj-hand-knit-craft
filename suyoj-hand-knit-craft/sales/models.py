from django.db import models
from invoice.models import InvoiceId
# Create your models here.


class Sales(models.Model):
    customer_name = models.CharField(max_length=120, default='Noname')
    GENDER_CHOICES = (('M', 'Male'), ('F', 'Female'),)
    gender = models.CharField(max_length=1,choices=GENDER_CHOICES, default='M')
    invoice_id = models.ForeignKey(InvoiceId, on_delete=models.CASCADE)
    total = models.IntegerField()

    def __str__(self):
        return str(self.id)


