from django.urls import path, include
from . import views
urlpatterns = [
    path('', views.HomePageView.as_view(), name='homepage'),
    path('product/', include('products.urls')),
    path('invoice/', include('invoice.urls')),
    path('sales/', include('sales.urls')),

]