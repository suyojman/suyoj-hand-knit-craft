from django.urls import path, include
from . import views

urlpatterns = [
    path('create', views.create_invoice, name='create_invoice'),
    path('delete/<int:id>', views.delete_invoice, name='delete_invoice'),
    path('add-product-to-invoice/<int:id>', views.add_product_to_invoice, name='add_product_to_invoice'),
    path('add-product-to-invoice/ajax/product_price', views.ajax_product_price, name='ajax_product_price'),
    path('add-product-to-invoice/ajax/quantity', views.ajax_product_quantity, name='ajax_product_quantity'),
    path('save', views.save_invoice, name='save_invoice'),
]
