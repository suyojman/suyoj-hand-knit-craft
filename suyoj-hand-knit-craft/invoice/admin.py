from django.contrib import admin
from .models import InvoiceId, InvoiceRow
# Register your models here.

admin.site.register(InvoiceId)
admin.site.register(InvoiceRow)
