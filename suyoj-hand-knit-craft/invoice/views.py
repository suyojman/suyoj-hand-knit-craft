from django.shortcuts import render, redirect
from .models import InvoiceId, InvoiceRow, Product
from sales.models import Sales
from .forms import InvoiceCreateForm
from django.http import JsonResponse
from django.db.models import Sum
# Create your views here.


def create_invoice(request):
    if request.method == "POST":
        create_invoice_id = InvoiceId.objects.create()
    last_invoice = InvoiceId.objects.all().last()
    invoice_row = InvoiceRow.objects.filter(invoice_id=last_invoice.id)
    total = InvoiceRow.objects.filter(invoice_id=last_invoice.id).aggregate(Sum('total'))
    return render(request, 'invoice/create_invoice.html', {'invoice_id': last_invoice, 'invoice_row': invoice_row, 'total': total})


def delete_invoice(request, id):
    invoice_row = InvoiceRow.objects.filter(invoice_id=id)
    if invoice_row.exists():
        invoice_id = InvoiceId.objects.get(id=id)
        invoice_id.delete()
        invoice_row.delete()
        return redirect('/')
    else:
        invoice_id = InvoiceId.objects.get(id=id)
        invoice_id.delete()
        return redirect('/')


def add_product_to_invoice(request, id):
    form = InvoiceCreateForm(request.POST or None, initial={'invoice_id': id})
    if request.method == "POST":
        if form.is_valid():
            print("Inside")
            form.save()
            return redirect('/invoice/create')
    return render(request, 'invoice/add_product_to_invoice.html', {'form': form, 'invoice_id': id})


def ajax_product_price(request):
    print(request.method)
    product_id = request.GET.get('product_id', None)
    print(product_id)
    p = Product.objects.get(id=product_id)
    data = {
        'price': p.price
    }
    return JsonResponse(data)


def ajax_product_quantity(request):
    product_id = request.GET.get('product_id', None)
    p = Product.objects.get(id=product_id)
    print(p)
    data = {
        'quantity_left': p.quantity,

    }
    return JsonResponse(data)


def save_invoice(request):
    customer_name = request.POST.get('customer_name', None)
    gender = request.POST.get('gender', None)
    invoice_id = request.POST.get('invoice_id', None)
    total = request.POST.get('total', None)
    invoice = InvoiceId.objects.get(id=invoice_id)
    individual_sale = Sales(customer_name=customer_name, gender=gender, invoice_id=invoice,total=total)
    individual_sale.save()
    return redirect('/')
