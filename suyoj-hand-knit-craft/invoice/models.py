from django.db import models
from datetime import datetime
from products.models import Product
# Create your models here.


class InvoiceId(models.Model):
    date_time = models.DateTimeField(default = datetime.now)

    def __str__(self):
        return str(self.id)


class InvoiceRow(models.Model):
    product_name = models.ForeignKey(Product, on_delete = models.CASCADE)
    invoice_id = models.ForeignKey(InvoiceId, on_delete=models.CASCADE)
    price = models.IntegerField()
    quantity = models.IntegerField()
    total = models.IntegerField()

    def __str__(self):
        return str(self.product_name)


