from django import forms
from .models import InvoiceRow


class InvoiceCreateForm(forms.ModelForm):
    price = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    total = forms.CharField(widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    quantity = forms.CharField(widget=forms.TextInput(attrs={'type': 'number', 'min': '1'}))

    class Meta:
        model = InvoiceRow
        fields = [
            'invoice_id',
            'product_name',
            'price',
            'quantity',
            'total',
        ]

    def clean_price(self):
        price = self.cleaned_data.get('price')
        if len(price) > 5:
            raise forms.ValidationError("Please enter valid price !!")
        else:
            return price

