from django import forms
from .models import Product


class ProductForm(forms.ModelForm):
    price = forms.CharField(widget=forms.TextInput(attrs={'type': 'number'}))
    quantity = forms.CharField(widget=forms.TextInput(attrs={'type': 'number'}))

    class Meta:
        model = Product
        fields = [
            'product_name',
            'quantity',
            'price',
            'product_code',
            'description',
        ]

    def clean_price(self):
        price = self.cleaned_data.get('price')
        if len(price)<6:
            return price
        else:
            raise forms.ValidationError("Please input valid price!!")

    def clean_product_name(self):
        product_name = self.cleaned_data.get('product_name')
        print(product_name)
        if Product.objects.filter(product_name=product_name).exists():
            print("Inside here !!!")
            raise forms.ValidationError("The product is already in the Product List!!"
                                        "Please check in the Stock Management list!!")
        else:
            return product_name


class UpdateForm(forms.ModelForm):
    price = forms.CharField(widget=forms.TextInput(attrs={'type': 'number'}))
    quantity = forms.CharField(widget=forms.TextInput(attrs={'type': 'number'}))

    class Meta:
        model = Product
        fields = [
            'product_name',
            'quantity',
            'price',
            'product_code',
            'description',
        ]

    def clean_price(self):
        price = self.cleaned_data.get('price')
        if len(price)<6:
            return price
        else:
            raise forms.ValidationError("Please input valid price!!")

