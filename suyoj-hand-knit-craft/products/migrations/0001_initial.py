# Generated by Django 2.1.5 on 2019-01-27 09:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('product_name', models.CharField(max_length=120)),
                ('quantity', models.IntegerField()),
                ('price', models.IntegerField()),
                ('product_code', models.CharField(max_length=15)),
                ('description', models.CharField(max_length=200)),
            ],
        ),
    ]
