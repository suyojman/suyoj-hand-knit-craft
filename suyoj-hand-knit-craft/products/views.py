from django.shortcuts import render, redirect, get_object_or_404
from .forms import ProductForm, UpdateForm
from .models import Product
from django.views.generic import ListView
# Create your views here.


def product_create_view(request):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/')
    else:
        return render(request, 'products/create_product.html', {'form': form})


def product_update_view(request, id):
    product = Product.objects.get(id=id)
    form = UpdateForm(request.POST or None, instance=product)
    if form.is_valid():
        form.save()
        return redirect("/")

    return render(request, 'products/update_product.html', {'form': form, 'product': product})


def product_delete_view(request, id):
    product_delete = get_object_or_404(Product, id=id)
    if request.method == "POST":
        product_delete.delete()
        return redirect('/')

    return render(request, 'products/delete_product.html', {'object': product_delete})


class ProductListView(ListView):
    model = Product
    template_name = 'products/stock.html'

