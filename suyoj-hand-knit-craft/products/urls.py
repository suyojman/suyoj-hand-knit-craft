from django.urls import path, include
from . import views
urlpatterns = [
    path('create', views.product_create_view, name='create'),
    path('<int:id>/update', views.product_update_view, name='update'),
    path('stock', views.ProductListView.as_view(), name='stock'),
    path('<int:id>/delete', views.product_delete_view, name='delete'),
]